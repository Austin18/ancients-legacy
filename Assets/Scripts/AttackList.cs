﻿using UnityEngine;
using System.Collections;

public class AttackList : MonoBehaviour
{

    public float moveTime;
    protected Animator animationController;
    protected AttackManager attackManager;
    protected Vector3 startPos;
    public float speed;
    public Transform Target;
    public GameObject projectile2;
    public LevelManager manager;
    public Transform Firepoint;
    public GameObject projectile;
    public GameObject projectile3;

    protected bool actionActive;


    private void Awake()
    {
        
    }
    // Use this for initialization
    void Start()
    {
        attackManager = GetComponent<AttackManager>();
        
    }

    public void SlashAttack()
    {
        if (actionActive == false)
        {
            actionActive = true;

            StartCoroutine(SlashAttackAction());
        }
    }

    IEnumerator SlashAttackAction()
    {
        while(true)
        {
            if (manager.turnPhase == TurnPhase.TARGETSELECTED) break;

            yield return new WaitForEndOfFrame();
        }

        manager.turnPhase = TurnPhase.ACTION;

        // do the action

        StartCoroutine(callEndTurn());
    }

    public void ProjectileAttack()
    {
        if (actionActive == false)
        {
            actionActive = true;
            StartCoroutine(ProjectileAttackAction());
        }
    }

    IEnumerator ProjectileAttackAction()
    {
        while (true)
        {
            if (manager.turnPhase == TurnPhase.TARGETSELECTED) break;

            yield return new WaitForEndOfFrame();
        }

        manager.turnPhase = TurnPhase.ACTION;

        Target = attackManager.Target;

       




        GameObject go = Instantiate(projectile2);
        go.transform.position = Firepoint.position;
        go.transform.rotation = Quaternion.LookRotation(Target.position - Firepoint.position);

        StartCoroutine(callEndTurn());
    }

    public void ProjectileAttack2()
    {
        if (actionActive == false)
        {
            actionActive = true;
            StartCoroutine(ProjectileAttack2Action());
        }
    }

    IEnumerator ProjectileAttack2Action()
    {
        while (true)
        {
            if (manager.turnPhase == TurnPhase.TARGETSELECTED) break;

            yield return new WaitForEndOfFrame();
        }

        manager.turnPhase = TurnPhase.ACTION;

        Target = attackManager.Target;
      
       

            GameObject go = Instantiate(projectile3);
            go.transform.position = Firepoint.position;
            go.transform.rotation = Quaternion.LookRotation(Target.position - Firepoint.position);
        

        

        StartCoroutine(callEndTurn());
    }

    public void AIProjectileAttack()
    {
        Target = manager.EnemyRange[Random.Range(0, manager.EnemyRange.Count)].transform;
        
        

        manager.turnPhase = TurnPhase.TARGETSELECTED;

        manager.turnPhase = TurnPhase.ACTION;

        GameObject go = Instantiate(projectile);
        go.transform.position = Firepoint.position;
        go.transform.rotation = Quaternion.LookRotation(Firepoint.position - Target.position);

        StartCoroutine(callEndTurn());
    }

    IEnumerator moveToPosition(Vector3 pos)

    {
            Vector3 curPosition = transform.position;
            float i = 0.0f;
            float rate = 1.0f / moveTime;

            while (i < 1.0f)
            {
                i += Time.deltaTime * rate;

                transform.position = Vector3.Lerp(curPosition, pos, i);
                yield return null;
            }
        }

        IEnumerator doTarget(Transform target)
    {
            bool attacking = true;

            while (attacking)
            {
            // do attack, when finished set attacking to false
            
                animationController.SetTrigger("Attack");

                Debug.Log("attack1");
            yield return new WaitForSeconds(2);
            attacking = false;
                Debug.Log(attacking);
                yield return null;
            }
        }

        IEnumerator attackTarget()
     {
            if (attackManager.Target != null)
            {
                Transform targetOffset = attackManager.Target.FindChild("Attackoffset");
                startPos = transform.position;
              

                while (true)
                {
                    yield return StartCoroutine(moveToPosition(targetOffset.position));
                    yield return StartCoroutine(doTarget(Target));
                    yield return StartCoroutine(moveToPosition(startPos));
                    yield return StartCoroutine(callEndTurn());
                    break;
                }
            }
        }


     void Update()
    {
     
        if (manager.curCharacter == null)
        { }
        else
        {
            animationController = manager.curCharacter.GetComponent<Animator>();
        }

        if (manager == null)
        {
            manager = GameObject.FindWithTag("Gamemanager").GetComponent<LevelManager>();
        }
    }



    IEnumerator callEndTurn()
    {
        if (manager.HeroChar != null)
        {
            manager.HeroChar.deactivateUI();
        }

        while (true)
        {
            yield return new WaitForSeconds(1);
            break;
        }

        actionActive = false;

        manager.endTurn();
        yield return null;      
    }

}



