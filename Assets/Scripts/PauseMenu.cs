﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class PauseMenu: MonoBehaviour
{
    public GameObject OverlayCanvas;
    public GameObject OpenMenu;
    List<BaseHero> heroes;
    GameObject[] heroChars;
    // Use this for initialization
    void Start()
    {
        
    }

    public void openmenu()
    {
        OverlayCanvas.SetActive(false);
        OpenMenu.SetActive(true);
    }

    public void Resume()
    {
        OverlayCanvas.SetActive(true);
        OpenMenu.SetActive(false);
    }

    public void Restart()
    {

        List<BaseHero> heroes = new List<BaseHero>();

        GameObject[] heroChars = GameObject.FindGameObjectsWithTag("Hero");

        foreach (GameObject heroGO in heroChars)
        {
            BaseHero baseHero = heroGO.GetComponent<BaseHero>();

            if (baseHero != null)
            {
                heroes.Add(baseHero);
            }
        }

        foreach (BaseHero character in heroes)
        {
            Destroy(character.gameObject);
        }


        SceneManager.LoadScene("Level 1");
    }
    public void ReturntoMainMenu()
    {
        List<BaseHero> heroes = new List<BaseHero>();

        GameObject[] heroChars = GameObject.FindGameObjectsWithTag("Hero");

        foreach (GameObject heroGO in heroChars)
        {
            BaseHero baseHero = heroGO.GetComponent<BaseHero>();

            if (baseHero != null)
            {
                heroes.Add(baseHero);
            }
        }

        foreach (BaseHero character in heroes)
        {
            Destroy(character.gameObject);
        }

        SceneManager.LoadScene("Main Menu");
    }
    // Update is called once per frame



}
