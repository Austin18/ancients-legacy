﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Defeat : MonoBehaviour {

	// Use this for initialization
	void Start () {
    
        StartCoroutine(ReturntoMenu());
        
    }
    IEnumerator ReturntoMenu()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Main Menu");
    }    
}
