﻿using UnityEngine;
using System.Collections;

public class AttackManager : MonoBehaviour
{
    public Transform Target;
    public Camera cam;
    RaycastHit hit;
    public float moveTime;
    protected Transform startPos;
    Ray ray;
    public LevelManager manager;
    //assign variable to list of enemies in game level. 

    private void Awake()
    {
        
    } 
    void Start()
    {
       

    }

    void Update()
    {
        if(cam == null)
        {
            cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        }

        if (manager == null)
        {
            manager = GameObject.FindWithTag("Gamemanager").GetComponent<LevelManager>();
        }

        if ((manager.turnPhase == TurnPhase.INPROGRESS) && Input.GetButtonDown("Fire1"))
        {
          
                ray = cam.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Enemy")
                {
                 manager.curCharacter.GetComponent<AttackManager>().Target  = hit.transform;

                    manager.turnPhase = TurnPhase.TARGETSELECTED;
                }
            }
        
    }
}