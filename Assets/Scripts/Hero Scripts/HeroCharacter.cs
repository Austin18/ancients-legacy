﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HeroCharacter : BaseHero
{
    public float moveTime;
    protected Vector3 startPos;
    public GameObject projectile2;
    public GameObject yourCanvas;
    public float rotatespeed;
    public LevelManager manager;
   

    private void Awake()
    {
      
    }

    void Start()
    {

      

        DontDestroyOnLoad(gameObject);
       
        

    }

    public void attack()
    {

        yourCanvas.SetActive(true);
        
    }

    public void deactivateUI()
    {
        yourCanvas.SetActive(false);
    }


    public bool Death()
    {
     if (health <= 0)
        {
            dead = true;
        }
        return dead;
    }

    void Update()
    {
      


        /*  scene = SceneManager.GetActiveScene();
          sceneName = scene.name;


          if (sceneName == "Victory")
          {
              Destroy(gameObject);
          }*/

        if (manager == null)
        {
            manager = GameObject.FindWithTag("Gamemanager").GetComponent<LevelManager>();
        }
        if (Death())
        {
            Destroy(gameObject);
            manager.LossCheck();
        }

        

    }
}





