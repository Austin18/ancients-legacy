﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Victory : MonoBehaviour {

	// Use this for initialization
	void Start () {
        List<BaseHero> heroes = new List<BaseHero>();

        GameObject[] heroChars = GameObject.FindGameObjectsWithTag("Hero");

        foreach (GameObject heroGO in heroChars)
        {
            BaseHero baseHero = heroGO.GetComponent<BaseHero>();

            if (baseHero != null)
            {
                heroes.Add(baseHero);
            }
        }

        foreach (BaseHero character in heroes)
        {
            Destroy(character.gameObject);
        }


        StartCoroutine(ReturntoMenu());


    }
    IEnumerator ReturntoMenu()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Main Menu");
    }    
}
