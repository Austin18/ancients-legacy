﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

public enum TurnPhase { STARTED, INPROGRESS, TARGETSELECTED, ACTION, ENDED };

public class LevelManager : MonoBehaviour
{
    public List<BaseHero> heroes;
    public List<BaseHero> EnemyRange;
    List<BaseEnemy> enemies;
    List<BaseCharacter> allCharacters;
    public BaseCharacter curCharacter;
    public HeroCharacter HeroChar;
    public int enemiesDead;
    public int heroesDead;
    public int winCondition;
    public int lossCondition;
    int sceneNumber;
    Scene scene;
    string sceneName;
    public int wincheck;

    public TurnPhase turnPhase = TurnPhase.ENDED;

    private void Awake()
    {
        
    }
    // Use this for initialization

    void Start()
    {

        scene = SceneManager.GetActiveScene();
        sceneName = scene.name;
        heroes = new List<BaseHero>();
        enemies = new List<BaseEnemy>();
        allCharacters = new List<BaseCharacter>();

        GameObject[] gos = GameObject.FindGameObjectsWithTag("Hero");
        
        foreach(GameObject go in gos)
        {
            BaseHero character = go.GetComponent<BaseHero>();
            heroes.Add(character);
            addCharacter(character);
        }

        

        gos = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject go in gos)
        {
            BaseEnemy character = go.GetComponent<BaseEnemy>();
            enemies.Add(character);
            addCharacter(character);
        }

        

        winCondition = enemies.Count;

        lossCondition = heroes.Count;

        EnemyRange = new List<BaseHero>(heroes);

        resortCharacters();

        takeTurn();
    }

    public void addCharacter(BaseCharacter character)
    {
        allCharacters.Add(character);
    }

    public void resortCharacters()
    {
        allCharacters = allCharacters.OrderByDescending(o => o.accumulatedTime).ToList();
    }

    public void takeTurn()
    {
        turnPhase = TurnPhase.STARTED;

        curCharacter = allCharacters[0];

        if (curCharacter.accumulatedTime >= 100)
        {
            turnPhase = TurnPhase.INPROGRESS;

            // select that character
            // if that character is a hero character, wait for player to choose an action
            // else call that character's AI controller  

            BaseHero hero = curCharacter as BaseHero;

            if (hero != null)
            {

                HeroChar = curCharacter as HeroCharacter;

                HeroChar.attack();

                // call overriden attack method in individual hero character class
                // this method sets up UI options for player
                // when that attack is over, call the GameManager.endTurn method
            }
            else
            {
                BaseEnemy enemy = curCharacter as BaseEnemy;

                if (enemy != null)
                {
                    enemy.AIAttack();
                    // call overriden attack method in individual enemy class
                    // this calls on the indicudual AI controller (or picks from a list of attacks) attached to the individual enemy character class
                    // when that attack is over, call the GameManager.endTurn method
                }
            }
        }
        else
        {
            curCharacter = null;
            endTurn();
        }
    }

    public void endTurn()
    {
        // check if one team (or both) are dead and determine winner etc.
        

        foreach (BaseEnemy Character in enemies)
        {
            if (Character.dead == true && allCharacters.Contains(Character))
            {
                allCharacters.Remove(Character);
                enemiesDead++;
                resortCharacters();
                WinCheck();
            }
        }

        foreach (BaseHero Character in heroes)
        {
            if (Character.dead == true && allCharacters.Contains(Character))
            {
                allCharacters.Remove(Character);
                heroesDead++;
                EnemyRange.Remove(Character);
                resortCharacters();
                LossCheck();
            }
        }

        if (curCharacter != null)
        {
           
            curCharacter.accumulatedTime = 0;
        }

        foreach(BaseCharacter character in allCharacters)
        {
            character.accumulatedTime += character.speed;
        }

        resortCharacters();

        turnPhase = TurnPhase.ENDED;     
    }

    public void WinCheck()
    { 
           
        if (enemiesDead == winCondition)
        {

            SetScene();

            if (sceneNumber % 4 != 0)
            {
                SceneManager.LoadScene(sceneName);
                
            }
            else
            {


                clearScene();
            }

        }

        


    }

    public void LossCheck()
    {
        
        if (heroesDead == lossCondition)
        {
            

            SceneManager.LoadScene("Defeat");
        }




    }

    void Update()
    {
        if (turnPhase == TurnPhase.ENDED)
        {
            takeTurn();
        }
    }

    void SetScene()
    {
       
       string[] words = sceneName.Split(' ');
        
       int.TryParse(words[1], out sceneNumber);
        sceneNumber++;
        
       words[1] = sceneNumber.ToString();
        sceneName = string.Join(" ", words);
        Debug.Log(sceneNumber);
        Debug.Log(sceneName);
    }

    void clearScene()
    {
        EnemyRange.Clear();

        SceneManager.LoadScene("Victory");
    }
}
