﻿using UnityEngine;
using System.Collections;

public class Projectile2 : MonoBehaviour
{
    public float speed;
    public int damage = 20;

    void Update()
    {
        transform.position = transform.position - (transform.forward * speed * Time.deltaTime);
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hero")
        {

            other.GetComponent<HeroCharacter>().health -= damage;
            Destroy(gameObject);

        }
    }
}
