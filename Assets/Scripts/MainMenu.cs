﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject MenuCanvas;
    public GameObject StartGameCanvas;
    public GameObject ComingSoonCanvas;

    // Use this for initialization
    void Start () {
	
	}

    public void StartGame()
    {
    StartGameCanvas.SetActive(false);
    MenuCanvas.SetActive(true);
}

    public void LoadLevel1()
    {
        SceneManager.LoadScene("Level 1");
    }
    public void NotAvailable()
    {
        MenuCanvas.SetActive(false);
        ComingSoonCanvas.SetActive(true);
    }
    public void Returntomenu()
    {
        ComingSoonCanvas.SetActive(false);
        MenuCanvas.SetActive(true);
    }
        // Update is called once per frame



    }
