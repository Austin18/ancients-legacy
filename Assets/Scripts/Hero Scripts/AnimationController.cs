﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {

    protected Animator animationController;
  
     


    // Use this for initialization
    void Start () {
        animationController = GetComponent<Animator>();


    }
	
	// Update is called once per frame
	void Update () {

        animationController.SetTrigger("Attack");

    }
}
