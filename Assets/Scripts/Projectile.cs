﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	public float speed;
    public int damage = 10;
						
	void Update () 
	{
		transform.position = transform.position + (transform.forward * speed * Time.deltaTime);
	}

	void OnBecameInvisible()
	{
		Destroy(gameObject);
	}
			
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy")
		{

           other.GetComponent<BaseEnemy>().health -= damage;
            Destroy(gameObject);
            
		}
	}
}
